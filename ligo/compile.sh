#!/bin/bash
# chmod u+x compile.sh

# last compiled with
# ligo --version
# Commit SHA: 10767f3ff3c6ab9b128266be0c0f7965377c7f26
# Commit Date: 2020-09-18 21:12:22 +0000

# this script depends on:
# - dos2unix

ligo compile-contract ./fa1.2.ligo main | dos2unix > contracts/fa1.2.tz
ligo compile-contract --michelson-format=json ./fa1.2.ligo main | dos2unix > contracts/fa1.2.json
