// FA1.2

// a token ledger that identifies Tezos addresses to token balances
// it provides functionality for transferring, approval and data querying
// implemented in PascalLigo

// copyright: camlCase 2019-2020
// version: 0.4.0.0
// license: GPLv3

// References

// the FA1 standard
// https://gitlab.com/tzip/tzip/-/blob/2bedaa85c445732b9bf8914bf29cb9dc5fb2cf4f/proposals/tzip-5/tzip-5.md

// the FA1.2 standard
// https://gitlab.com/tzip/tzip/-/blob/2bedaa85c445732b9bf8914bf29cb9dc5fb2cf4f/proposals/tzip-7/tzip-7.md

// known attack vector for approve. This vulnerability exists in FA1.2.
// https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit

// an internal store type use to force the structure of the storage
type s is record
  total_supply: nat; // the total supply of token, this must equal the sum of all balances in the accounts
end

// an account form the ledger
type account is record
  balance : nat;
  allowances: map(address, nat);
end

// the main store type, separated to force accounts as the top left item of the
// storage so we can use the old big_map query
type storage is record
  accounts: big_map(address, account); // the ledger or collection of accounts
  s: s
end

// the entrypoints
type action is
| Transfer of (address * (address * nat))
| Approve of (address * nat)
| GetAllowance of (address * address * contract(nat))
| GetBalance of (address * contract(nat))
| GetTotalSupply of (unit * contract(nat))

function is_allowed (const owner : address ; const value : nat ; var s : storage) : bool is 
  block {
    var allowed: bool := False;
    if sender =/= owner then block {
      // Checking if the sender is allowed to spend in name of owner
      case (s.accounts[owner]) of
        | None      -> failwith("is_allowed: the owner does not own any tokens.")
        | Some(owner_account) -> block {
          case (owner_account.allowances[sender]) of
            | None -> allowed := False
            | Some(allowance_amount) -> allowed := allowance_amount >= value
          end;
        }
      end;
    };
    else allowed := True;
  } with allowed

function get_account (const owner : address ; const accounts: big_map(address, account)) : account is
  block { skip } with
    (case (accounts[owner]) of
      | Some(a) -> a
      | None    -> (failwith("Owner does not have a balance") : account)
     end)

// Transfer a specific amount of tokens from the accountFrom address to a destination address
// Pre conditions:
//  The sender address is the account owner or is allowed to spend x in the name of accountFrom
//  The owner account has a balance greater than or equal to value
// Post conditions:
//  The balance of owner is decreased by amount
//  The balance of destination is increased by amount
//  The allowance of the sender is decreased if the sender is not the owner
function transfer (const owner      : address;
                   const to_        : address;
                   const value      : nat    ;
                   var storage      : storage) :
                   storage is
 block {
  // If owner = destination transfer is not necessary
  if owner = to_ then skip;
  else block {
    // Is sender allowed to spend value in the name of owner
    if is_allowed(owner, value, storage) then skip else {
      failwith ("NotEnoughAllowance")
    };
    
    // fetch owner account
    const owner_account: account = get_account(owner, storage.accounts);

    // check that the owner can spend that much
    if value > owner_account.balance 
    then failwith ("NotEnoughBalance");
    else skip;

    // check that the allowance is large enough, if the owner is the sender then it is the
    // balance
    var allowance_amount : nat := 0n;
    if (owner =/= sender) then block {
      case (owner_account.allowances[sender]) of
        | Some(a) -> allowance_amount := a
        | None    -> failwith("Sender does not have an allowance for this owner")
      end
    } else block {
      allowance_amount := owner_account.balance;
    };

    if value > allowance_amount
    then failwith ("NotEnoughAllowance");
    else skip;

    // update the owner's balance
    // calculate new_balance, convert int to nat
    const new_balance_int: int = owner_account.balance - value;
    if (new_balance_int >= 0) then block {
      owner_account.balance := abs(new_balance_int);
    } else block {
      failwith("balance - value is negative.")
    };

    // decrease the allowance of sender in owner account if necessary
    if (owner =/= sender) then block {
       const new_allowance_int: int = allowance_amount - value;
       if (new_allowance_int >= 0) then block {
         owner_account.allowances[sender] := abs(new_allowance_int);
       } else block {
         failwith("allowance - value is negative.")
       };
    } else skip;
    storage.accounts[owner] := owner_account;

    // create an empty to_account
    var to_account: account := record 
      balance = 0n;
      allowances = (map end : map(address, nat));
    end;

    // fetch the account for the to_ address account
    case storage.accounts[to_] of
    | None    -> skip
    | Some(n) -> to_account := n
    end;

    // update the to_account balance, set it in accounts
    to_account.balance  := to_account.balance + value;
    storage.accounts[to_] := to_account;
  }
 } with storage

// Sender approves an amount to be spent by spender
// Pre conditions:
//  None
// Post conditions:
//  The allowance of spender in the name of sender is value
function approve (const spender : address;
                  const value   : nat    ;
                  var   storage : storage) :
                  storage is
 block {
  const owner_account: account =
    (case (storage.accounts[sender]) of
      | Some(a) -> a
      | None    ->
          (record 
            balance = 0n;
            allowances = (map end : map(address, nat));
           end)
     end);

  case (owner_account.allowances[spender]) of
    | Some(spender_amount) -> block {
        if (spender_amount =/= 0n) then {
          if (value =/= 0n) then {
            failwith("UnsafeAllowanceChange");
          } else skip;          
        } else skip;
      }
    | None -> skip
  end;
  
  owner_account.allowances[spender] := value;
  storage.accounts[sender]          := owner_account;
 } with storage

// View function that forwards the allowance amount of spender in the name of tokenOwner to a contract
// Pre conditions:
//  None
// Post conditions:
//  The state is unchanged
function get_allowance (const owner   : address      ;
                        const spender : address      ;
                        const contr   : contract(nat);
                        var   storage : storage      ) :
                        list(operation) is
 block {
  const owner_account: account = get_account(owner, storage.accounts);
  var spender_allowance : nat := 0n;
  case (owner_account.allowances[spender]) of
    | Some(a) -> spender_allowance := a
    | None    -> failwith("Spender does not have an allowance for this owner")
  end
 } with list [transaction(spender_allowance, 0tz, contr)]

// View function that forwards the balance of owner to a contract
// Pre conditions:
//  None
// Post conditions:
//  The state is unchanged
function get_balance (const owner : address ;
                      const contr : contract(nat) ;
                      var   storage : storage) :
                      list(operation) is
 block {
  const owner_account: account = get_account(owner, storage.accounts);
 } with list [transaction(owner_account.balance, 0tz, contr)]

// View function that forwards the total_supply to a contract
// Pre conditions:
//  None
// Post conditions:
//  The state is unchanged
function get_total_supply (const contr : contract(nat) ;
                           const storage : storage) :
                           list(operation) is
  list [transaction(storage.s.total_supply, 0tz, contr)]

function main (const p : action ;
               const s : storage) :
  (list(operation) * storage) is
 block { 
   // Reject any transaction that try to transfer token to this contract
   if amount =/= 0tz then failwith ("This contract do not accept XTZ");
   else skip;
  } with case p of
  | Transfer(n) -> ((nil : list(operation)), transfer(n.0, n.1.0, n.1.1, s))
  | Approve(n) -> ((nil : list(operation)), approve(n.0, n.1, s))
  | GetAllowance(n) -> (get_allowance(n.0, n.1, n.2, s), s)
  | GetBalance(n) -> (get_balance(n.0, n.1, s), s)
  | GetTotalSupply(n) -> (get_total_supply(n.1, s), s)
 end