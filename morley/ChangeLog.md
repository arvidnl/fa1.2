# Changelog for fa-one-point-two

## 0.6.0.0 -- 2020-09-22
* Fix order of getAllowance params.

## 0.5.0.0 -- 2020-09-04
* Conform to tzip-7 expectations about approve entrypoint behavior. If allowance
  is zero it can be set to non-zero. If allowance is non-zero it can only be set
  to zero.

## 0.4.0.0 -- 2020-07-21
* Add executable code to create contract.
* Fix bug with approval in transfer.
* Add some property tests.

## 0.3.0.0 -- 2020-07-21
* Move test code into the main library so it is reusable.

## 0.2.0.0 -- 2020-07-17
* Add error message for intToNatural.
* Fix arithmetic error in transfer.
* Add some simple unit tests for transfer and approve entrypoints.
