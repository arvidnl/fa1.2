{-# LANGUAGE OverloadedStrings #-}

-- tezos-client typecheck script fa1.2.tz

module Main where

import qualified Data.Map as Map
import           Lorentz.ContractRegistry
import qualified FaOnePointTwo.Contract as FA

main :: IO ()
main = do
  let contractRegistry =
        ContractRegistry $ Map.fromList
        [ "fa1.2" ?:: ContractInfo
          { ciContract      = FA.contract
          , ciIsDocumented  = True
          , ciStorageParser = Nothing
          , ciStorageNotes  = Nothing
          }    
        ]

  runContractRegistry contractRegistry (Print "fa1.2" (Just "fa1.2.tz") False False)
