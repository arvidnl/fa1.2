{-|
Module      : FaOnePointTwo.Test
Copyright   : (c) camlCase, 2020

Values and functions to test the FA1.2 contract.

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE TypeSynonymInstances      #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module FaOnePointTwo.Test where

import qualified Data.Map.Strict as Map
import qualified FaOnePointTwo.Contract as FA
import           Lorentz hiding (amount, balance)
import           Unsafe.Coerce (unsafeCoerce)
import           Util.Named ((.!))
import           Prelude (fst, snd)

-- =============================================================================
-- Make parameter values
-- =============================================================================

mkApproveParams :: Address -> Natural -> FA.ApproveParams
mkApproveParams spender value =
  ( #spender .! spender
  , #value   .! value
  )

mkGetAllowanceParams :: Address -> Address -> ContractRef Natural -> FA.GetAllowanceParams
mkGetAllowanceParams owner spender contr =
  ( #ownerSpender .! (#owner   .! (#owner .! owner), #spender .! spender)
  , #contr   .! contr
  )

mkGetBalanceParams :: Address -> ContractRef Natural -> FA.GetBalanceParams
mkGetBalanceParams owner contr =
  ( #owner .! (#owner .! owner)
  , #contr .! contr
  )

mkGetTotalSupplyParams :: ContractRef Natural -> FA.GetTotalSupplyParams
mkGetTotalSupplyParams contr =
  ( #unit  .! ()
  , #contr .! contr
  )

mkTransferParams :: Address -> Address -> Natural -> FA.TransferParams
mkTransferParams owner to value =
  ( #owner .! (#owner .! owner)
  , #to    .! (#receiver .! to)
  , #value .! value
  )

-- =============================================================================
-- Contract storage for testing
-- =============================================================================

-- | No accounts and 0 totalSupply
emptyStorage :: FA.Storage
emptyStorage =
  FA.Storage
    (BigMap Map.empty)
    (FA.StorageFields 0)

-- | Create storage for a given address and total supply. The entirety of the
-- total supply is given to the provided address. No approvals are included.
initStorage :: Address -> Natural -> FA.Storage
initStorage addr total =
  FA.Storage
    (BigMap $ Map.fromList [(addr, setAccountBalance total emptyAccount)])
    (FA.StorageFields total)

-- | An account with 0 blaance and no approvals
emptyAccount :: FA.Account
emptyAccount = (#balance .! 0, #approvals .! Map.empty)

-- | Set the balance of an account
setAccountBalance :: Natural -> FA.Account -> FA.Account
setAccountBalance n (_, approvals) = (#balance .! n, approvals)

-- | Insert an approval into an account with existing approvals
insertAllowance :: Address -> Natural -> FA.Account -> FA.Account
insertAllowance spender amount (balance, approvalsNamed) =
  let approvals = unsafeCoerce approvalsNamed :: Map.Map Address Natural in
  (balance, #approvals .! Map.insert spender amount approvals)

-- | Insert an account into an existing storage.
insertAccount :: Address -> FA.Account -> FA.Storage -> FA.Storage
insertAccount addr account (FA.Storage (BigMap m) storageFields) =
  FA.Storage (BigMap $ Map.insert addr account m) storageFields

-- | Get the Total Supply of the contract.
getTotalSupply :: FA.Storage -> Natural
getTotalSupply (FA.Storage _ storageFields) =
  unsafeCoerce $ FA.totalSupply storageFields

-- | Get the Total Supply of the contract.
getBalance :: Address -> FA.Storage -> Maybe Natural
getBalance owner (FA.Storage (BigMap bigmap) _) =
  case Map.lookup owner bigmap of
    Nothing -> Nothing
    Just account -> unsafeCoerce . fst $ account

-- | Get the Total Supply of the contract.
getAllowance :: Address -> Address -> FA.Storage -> Maybe Natural
getAllowance owner spender (FA.Storage (BigMap bigmap) _) =
  case Map.lookup owner bigmap of
    Nothing -> Nothing
    Just account ->
      case Map.lookup spender (unsafeCoerce . snd $ account :: FA.Approvals) of
        Nothing -> Nothing
        Just allowance -> Just allowance
