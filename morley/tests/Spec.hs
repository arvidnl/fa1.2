{-|
Module      : Main
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io
-}

module Main where

import qualified Spec.Transfer as Transfer
import Test.Hspec                 (hspec, parallel)

main :: IO ()
main = do
  hspec $ parallel Transfer.spec
