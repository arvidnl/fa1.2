{-|
Module      : Test.Dexter.Contract.Default
Copyright   : (c) camlCase, 2020
Maintainer  : james@camlcase.io

Unit and property tests for the transfer entrypoint in FA1.2.

-}

{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Spec.Transfer where

-- fa1.2
import qualified FaOnePointTwo.Test     as FA
import qualified FaOnePointTwo.Contract as FA
import qualified Spec.Mock              as Mock

-- lorentz and morley
import           Lorentz         ( Address, HasDefEntryPointArg, HasEntryPointArg
                                 , Mutez, IsoValue, ToTAddress, mt, zeroMutez) 
import           Lorentz.Test
import           Michelson.Runtime.GState (GState(..), AddressState(..))
import           Util.Named ((.!))

-- testing
import           Test.Hspec      (Spec, it)
import           Test.QuickCheck (Gen, choose, forAll)
import qualified Data.Map as Map

spec :: Spec
spec = do
  it "unit: transfer to self does not change the storage" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Transfer") (FA.mkTransferParams Mock.alice Mock.alice 100) zeroMutez

      lExpectStorageConst fa storage

  it "unit: transfer to another address decreases the owner's balance and increases the receiver's balance" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Transfer") (FA.mkTransferParams Mock.alice Mock.bob 100) zeroMutez

      let expectedStorage =
            FA.insertAccount Mock.alice (FA.setAccountBalance 999900 FA.emptyAccount) $
            FA.insertAccount Mock.bob (FA.setAccountBalance 100 FA.emptyAccount)
            storage

      lExpectStorageConst fa expectedStorage

  it "unit: an owner cannot transfer more than their balance" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Transfer") (FA.mkTransferParams Mock.alice Mock.bob 5000000) zeroMutez
        `catchExpectedError` lExpectFailWith (== [mt|NotEnoughBalance|])  

  it "unit: approve amount for third party" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob 20000) zeroMutez

      let expectedStorage =
            FA.insertAccount Mock.alice (FA.insertAllowance Mock.bob 20000 $ FA.setAccountBalance 1000000 FA.emptyAccount)
            storage

      lExpectStorageConst fa expectedStorage

  it "unit: approve non-zero to zero" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob 20000) zeroMutez
      lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob 0) zeroMutez
      
      let expectedStorage =
            FA.insertAccount Mock.alice (FA.insertAllowance Mock.bob 0 $ FA.setAccountBalance 1000000 FA.emptyAccount)
            storage

      lExpectStorageConst fa expectedStorage

  it "unit: approve non-zero to non-zero will fail" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob 20000) zeroMutez
      lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob 30000) zeroMutez
        `catchExpectedError` lExpectFailWith (== [mt|UnsafeAllowanceChange|])  
      
  it "unit: third party can transfer approved amount and their allowance is updated correctly" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob 20000) zeroMutez
      lCallEPWithMutez Mock.bob fa (Call @"Transfer") (FA.mkTransferParams Mock.alice Mock.simon 20000) zeroMutez

      -- Alice should have 998,000 token
      -- Bob should have 0 allowance for Alice
      -- Simon should have 20,000 token
      let expectedStorage =
            FA.insertAccount Mock.alice (FA.insertAllowance Mock.bob 0 $ FA.setAccountBalance 980000 FA.emptyAccount) $
            FA.insertAccount Mock.simon (FA.setAccountBalance 20000 FA.emptyAccount) $  
            storage

      lExpectStorageConst fa expectedStorage

  it "unit: third party cannot transfer more than their approved amount" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob 20000) zeroMutez
      lCallEPWithMutez Mock.bob fa (Call @"Transfer") (FA.mkTransferParams Mock.alice Mock.simon 30000) zeroMutez
        `catchExpectedError` lExpectFailWith (== [mt|NotEnoughAllowance|])  

  it "unit: third party cannot transfer if they do not have an allowance" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

      lCallEPWithMutez Mock.bob fa (Call @"Transfer") (FA.mkTransferParams Mock.alice Mock.simon 30000) zeroMutez
        `catchExpectedError` lExpectFailWith (== [mt|NotEnoughAllowance|])  

  it "prop: approve amount for third party" $ do
    forAll ((,) <$> choose (10000,99999999999) <*> choose (0,99999999999)) $ \(totalSupplyInteger :: Integer, allowanceInteger :: Integer) ->
      integrationalTestProperty $ do
        
        let totalSupply = fromIntegral totalSupplyInteger :: Natural
            allowance   = fromIntegral allowanceInteger   :: Natural
            storage = FA.initStorage Mock.alice totalSupply

        fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

        lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob allowance) zeroMutez

        let expectedStorage =
              FA.insertAccount Mock.alice (FA.insertAllowance Mock.bob allowance $ FA.setAccountBalance totalSupply FA.emptyAccount)
              storage

        lExpectStorageConst fa expectedStorage

  it "prop: a spender cannot spend more than their allowance" $ do
    forAll genGreaterAndLesser $ \(greater, lesser) ->
      integrationalTestProperty $ do
    
        let storage = FA.initStorage Mock.alice greater
        fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

        lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob lesser) zeroMutez
        
        lCallEPWithMutez Mock.bob fa (Call @"Transfer") (FA.mkTransferParams Mock.alice Mock.bob greater) zeroMutez
          `catchExpectedError` lExpectFailWith (== [mt|NotEnoughAllowance|])  

  it "prop: a spender can spend less than or equal to their allowance" $ do
    forAll genBalanceAllowanceSpent $ \(balance_, allowance, spent) ->
      integrationalTestProperty $ do
    
        let storage = FA.initStorage Mock.alice balance_
        fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

        lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob allowance) zeroMutez
        lCallEPWithMutez Mock.bob fa (Call @"Transfer") (FA.mkTransferParams Mock.alice Mock.simon spent) zeroMutez

        let expectedStorage =
              FA.insertAccount Mock.simon (FA.setAccountBalance spent FA.emptyAccount)
              $ FA.insertAccount Mock.alice
                (  FA.insertAllowance Mock.bob (allowance - spent)
                  $ FA.setAccountBalance (balance_ - spent) FA.emptyAccount)
              storage

        lExpectStorageConst fa expectedStorage

  it "prop: approve amount for third party" $ do
    forAll ((,) <$> choose (10000,99999999999) <*> choose (0,99999999999)) $ \(totalSupplyInteger :: Integer, allowanceInteger :: Integer) ->
      integrationalTestProperty $ do
        
        let totalSupply = fromIntegral totalSupplyInteger :: Natural
            allowance   = fromIntegral allowanceInteger   :: Natural
            storage = FA.initStorage Mock.alice totalSupply

        fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

        lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob allowance) zeroMutez

        let expectedStorage =
              FA.insertAccount Mock.alice (FA.insertAllowance Mock.bob allowance $ FA.setAccountBalance totalSupply FA.emptyAccount)
              storage

        lExpectStorageConst fa expectedStorage

  it "prop: a spender cannot spend more than their allowance" $ do
    forAll genGreaterAndLesser $ \(greater, lesser) ->
      integrationalTestProperty $ do
    
        let storage = FA.initStorage Mock.alice greater
        fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

        lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob lesser) zeroMutez
        
        lCallEPWithMutez Mock.bob fa (Call @"Transfer") (FA.mkTransferParams Mock.alice Mock.bob greater) zeroMutez
          `catchExpectedError` lExpectFailWith (== [mt|NotEnoughAllowance|])  

  it "prop: a spender can spend less than or equal to their allowance" $ do
    forAll genBalanceAllowanceSpent $ \(balance_, allowance, spent) ->
      integrationalTestProperty $ do
    
        let storage = FA.initStorage Mock.alice balance_
        fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

        lCallEPWithMutez Mock.alice fa (Call @"Approve") (FA.mkApproveParams Mock.bob allowance) zeroMutez
        lCallEPWithMutez Mock.bob fa (Call @"Transfer") (FA.mkTransferParams Mock.alice Mock.simon spent) zeroMutez

        let expectedStorage =
              FA.insertAccount Mock.simon (FA.setAccountBalance spent FA.emptyAccount)
              $ FA.insertAccount Mock.alice
                (  FA.insertAllowance Mock.bob (allowance - spent)
                  $ FA.setAccountBalance (balance_ - spent) FA.emptyAccount)
              storage

        lExpectStorageConst fa expectedStorage

-- | Send mutez from the genesisAddress
lCallEPWithMutez
  :: forall cp epRef epArg toAddr.
     (HasEntryPointArg cp epRef epArg, IsoValue epArg, ToTAddress cp toAddr)
  => Address -> toAddr -> epRef -> epArg -> Mutez -> IntegrationalScenarioM ()
lCallEPWithMutez fromAddr toAddr epRef param mutez =
  lTransfer @cp @epRef @epArg
    (#from .! fromAddr) (#to .! toAddr)
    mutez epRef param

lCallDefWithMutez
  :: forall cp defEpName defArg toAddr.
     ( HasDefEntryPointArg cp defEpName defArg
     , IsoValue defArg
     , ToTAddress cp toAddr
     )
  => Address -> toAddr -> defArg -> Mutez -> IntegrationalScenarioM ()
lCallDefWithMutez fromAddr toAddr mutez =
  lCallEPWithMutez @cp @defEpName @defArg fromAddr toAddr CallDefault mutez
  

getGStateBalance :: Address -> GState -> Maybe Mutez
getGStateBalance addr gstate =
  case Map.lookup addr (gsAddresses gstate) of
    Just (ASSimple m) -> Just m
    _ -> Nothing

genGreaterAndLesser :: Gen (Natural, Natural)
genGreaterAndLesser = do
  x <- choose (2, 99999999999999) :: Gen Integer
  y <- choose (0, x-1)
  pure (fromIntegral x, fromIntegral y)

genBalanceAllowanceSpent :: Gen (Natural, Natural, Natural)
genBalanceAllowanceSpent = do
  x <- choose (2, 99999999999999) :: Gen Integer
  y <- choose (1, x)
  z <- choose (0, y)
  pure (fromIntegral x, fromIntegral y, fromIntegral z)
